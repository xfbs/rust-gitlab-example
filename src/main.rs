//! # Rust GitLab Example
//!
//! This implements a little command-line utility which will print a greeting for
//! a specified name.
use clap::Parser;

/// Command-line options
#[derive(Parser, Clone, Debug)]
pub struct Options {
    /// Name to use for greeting
    #[clap(long, short, default_value = "Human")]
    name: String,
}

/// Given a name, generate a greeting.
fn greeting(name: &str) -> String {
    format!("Hello {name}!")
}

#[test]
fn greeting_works() {
    assert_eq!(greeting("Mark"), "Hello Mark!".to_string());
}

fn main() {
    let options = Options::parse();
    println!("{}", greeting(&options.name));
}
