# Rust GitLab Example

This is an example of how a Rust project might be hosted by GitLab. This
repository contains some example Rust code, as well as a [CI configuration](.gitlab-ci.yml)
that will run the unit tests, build the project as well as the documentation and publish
the resulting binary and documentation on every push.

The latest build and documentation are available here:

- [rust-gitlab-example-amd64](https://xfbs.gitlab.io/rust-gitlab-example/rust-gitlab-example-amd64)
- [documentation](https://xfbs.gitlab.io/rust-gitlab-example/doc/rust_gitlab_example)
